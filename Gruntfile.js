const sass = require('node-sass');

module.exports = function(grunt){
	grunt.initConfig({
		sass: {
			options: {
				implementation: sass,
				outputStyle: 'compressed'
			},
			dist: {
				files: {
					'css/main.css': 'sass/main.scss'
				}
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			}
		}
	});
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default',['watch']);
}
